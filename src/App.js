import React from "react";

import { BrowserRouter, Route, Switch } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.css";
import "./assets/css/animate.css";
import "./assets/sass/light-bootstrap.scss?v=1.3.0";
import "./assets/css/demo.css";
import "./assets/css/pe-icon-7-stroke.css";
//import "./assets/css/icofont.css";
//import "./assets/css/icofont.min.css";

import AdminLayout from "./layouts/Admin";

const App = () => {
  return (
     <div className="App">
     <BrowserRouter>
      <Switch>
        <Route path="/" render={props => <AdminLayout {...props} />} />
      </Switch>
    </BrowserRouter>
    </div>
  );
};

export default App;
