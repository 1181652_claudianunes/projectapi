import React, { createContext, useState, useEffect } from "react";
import axios from "axios";
import {Alert} from "react-bootstrap";

export const CasinosContext = createContext();

export const CasinosProvider = ({ children }) => {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [casinos, setCasinos] = useState([]);
    
    //API - METHOD GET USING AXIOS
    useEffect(() => {
        const getData = async () => {
            try {
                const result = await axios ('/api/providers/icarus/casinos');
                if (result.status >= 300) {
                    throw new Error(result.statusText);
                }
                setCasinos(result.data);
                setLoading(false);
            }catch (e) {
                setError(e.message);
            }
        };
        getData();
    }, []);

    if (!!error) {
        return (
            <Alert variant='danger'>
                {error}
            </Alert>
        )
    };
    
    return (
        <div>
            <CasinosContext.Provider 
            value={{
                casinos, 
                loading
            }}
            >
            {children}
            </CasinosContext.Provider>
        </div>
    );
};

export default CasinosContext;

