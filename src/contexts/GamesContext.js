import React, { createContext, useState, useEffect } from "react";
import axios from "axios";

import { Alert } from "react-bootstrap";

export const GamesContext = createContext();

export const GamesProvider = ({ children }) => {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [games, setGames] = useState([]);

    //API - METHOD GET USING AXIOS
    useEffect(() => {
        const getData = async () => {
            try {
                const result = await axios ('/api/games');
                if (result.status >= 300) {
                    throw new Error(result.statusText);
                }
                setGames(result.data);
                setLoading(false);
            }catch (e) {
                setError(e.message);
            }
        };
        getData();
    }, []);

    if (!!error) {
        return (
            <Alert variant="danger">
                {error}
            </Alert>
        )
    };    
    
    return (
        <div>
            <GamesContext.Provider 
            value={{
                games, 
                loading,
                error,
                setError
            }}
            >
            {children}
            </GamesContext.Provider>
        </div>
    );
};

export default GamesContext;

