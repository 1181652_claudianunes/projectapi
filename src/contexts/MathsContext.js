import React, { createContext, useState, useEffect } from "react";
import axios from "axios";
import {Alert} from "react-bootstrap";

export const MathsContext = createContext();

export const MathsProvider = ({ children }) => {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [maths, setMaths] = useState([]);
    
    //API - METHOD GET USING AXIOS
    useEffect(() => {
        const getData = async () => {
            try {
                const result = await axios ('/api/maths');
                if (result.status >= 300) {
                    throw new Error(result.statusText);
                }
                setMaths(result.data);
                setLoading(false);
            }catch (e) {
                setError(e.message);
            }
        };
        getData();
    }, []);

    if (!!error) {
        return (
            <Alert variant='danger'>
                {error}
            </Alert>
        )
    };

    return (
        <MathsContext.Provider 
            value={{
                maths,
                loading
            }}
        >
        {children}
        </MathsContext.Provider>
    );
};

export default MathsContext;

