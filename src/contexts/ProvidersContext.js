import React, { createContext, useState, useEffect } from "react";
import axios from "axios";
import {Alert} from "react-bootstrap";

export const ProvidersContext = createContext();

export const ProvidersProvider = ({ children }) => {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [providers, setProviders] = useState(null);
    
    //API - METHOD GET USING AXIOS
    useEffect(() => {
        const getData = async () => {
            try {
                const result = await axios ('/api/providers');
                if (result.status >= 300) {
                    throw new Error(result.statusText);
                }
                setProviders(result.data);
                setLoading(false);
            }catch (e) {
                setError(e.message);
            }
        };
        getData();
    }, []);

    if (!!error) {
        return (
            <Alert variant='danger'>
                {error}
            </Alert>
        )
    };

    return (
        <ProvidersContext.Provider 
            value={{
                providers,
                setProviders,
                loading
            }}
        >
        {children}
        </ProvidersContext.Provider>
    );
};

export default ProvidersContext;

