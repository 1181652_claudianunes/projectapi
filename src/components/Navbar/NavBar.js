import React from 'react';
import { Nav, Navbar } from "react-bootstrap";

const NavBar = () => {
    return (
        <>
            <Navbar className="navBar" bg="dark" variant="dark">
                <Nav className="navLink flex-column">
                    <Nav.Link href="/">Home</Nav.Link>
                    <Nav.Link href="/games">Games</Nav.Link>
                    <Nav.Link href="/maths">Maths</Nav.Link>
                    <Nav.Link href="/providers">Providers</Nav.Link>
                    <Nav.Link href="/providers/icarus/casinos">Casinos</Nav.Link>
                </Nav>
            </Navbar>
        </>
    );
}

export default NavBar