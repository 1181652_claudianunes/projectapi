import React, { useState, useContext } from "react";
import axios from "axios";
import { Modal, ButtonGroup, Button, OverlayTrigger, Tooltip } from "react-bootstrap";

import { ProvidersContext } from "../../contexts/ProvidersContext";

import EditProvider from "../Modals/EditProvider";

const ProvidersInfo = (props) => {
    //providers by method GET from API (Context)
    const providersContext = useContext(ProvidersContext);
    const { providers, setProviders } = providersContext;

    //modal to edit provider and its provider fields (name, url, username, password)
    const [editModalShow, setEditModalShow] = useState(false);
    const [name, setName] = useState(props.value.name);
    const [url, setUrl] = useState(props.value.url);
    const [username, setUsername] = useState(props.value.username);
    const [password, setPassword] = useState(props.value.password);

    //modal to delete provider
    const [deleteModalShow, setDeleteModalShow] = useState(false);
    
    /*
    * Function to open the modal 
    */
    const handleShowModal = () => {
        setDeleteModalShow(true);
    }

    /*
    * Function to close the modal 
    */
    const handleCloseModal = (itemProvider) => {
        setDeleteModalShow(false);
    }

    /*
    * Function to show info of selected provider 
    */
    const getProviderInfo = (item) => {
            /*
            * Function to delete a provider
            */ 
            //API - METHOD DELETE USING AXIOS
            const deleteProvider = (() => {
                axios.delete(`/api/providers/${name}`)
                    .then(response => {
                        const deletedProviders = providers.filter((provider) => provider.name !== name);
                        setProviders(deletedProviders); 
                        props.onPreHide("success")
                        props.onHide();
                    })
                    .catch((err) => { 
                        props.onPreHide("error")
                        props.onHide();
                    }); 
            })

        /*
        * Function to edit a provider
        */ 
        const editProvider = (name) => {
            setEditModalShow(true)
            setName(item.name)
            setUrl(item.url)
            setUsername(item.username)
            setPassword(item.password)
        }

        return (
            <div>
                <Modal {...props}  className="app_modal">
                    <Modal.Header className="app_modal_header" closeButton>
                        <Modal.Title className="app_modal_header_title">
                            {name.toUpperCase()}
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body className="app_modal_body">
                        <EditProvider 
                            show={editModalShow}
                            onHide={() => {
                                setEditModalShow(false);
                            }}
                            id={item.id}
                            name={name}
                            url={url}
                            username={username}
                            password={password}
                        /> 
                        <div className="info_elements">
                            <h5 className="info_elements_title">URL:</h5>
                            <p className="info_elements_body">"{item.url}"</p>
                        </div>
                        <div className="info_elements">
                            <h5 className="info_elements_title">Username:</h5>
                            <p className="info_elements_body">"{item.username}"</p>
                        </div>
                        <div className="info_elements">
                            <h5 className="info_elements_title">Password:</h5>
                            <p className="info_elements_body">"{item.password}"</p>
                        </div>
                        <div>
                            <ButtonGroup aria-label="Basic example" className="buttons_edit_delete">
                                <OverlayTrigger
                                    trigger={['hover', 'focus']}
                                    placement="bottom"
                                    overlay={
                                        <Tooltip id={`tooltip-bottom`}>
                                            Edit   
                                        </Tooltip>
                                    }>
                                    <Button 
                                        variant="warning"
                                        className="pe-7s-pen"
                                        onClick={() => editProvider(item.name)}
                                    >
                                    </Button>
                                </OverlayTrigger>
                                <br />
                                <OverlayTrigger
                                    trigger={['hover', 'focus']}
                                    placement="bottom"
                                    overlay={
                                        <Tooltip id={`tooltip-bottom`}>
                                            Delete   
                                        </Tooltip>
                                    }>
                                    <Button 
                                        variant="danger"
                                        className="pe-7s-trash"
                                        onClick={handleShowModal}
                                    >
                                    </Button>
                                </OverlayTrigger>
                            </ButtonGroup>   
                            <Modal 
                                className="app_modal"
                                show={deleteModalShow} 
                                onHide={(e) => {
                                    handleCloseModal(item.name);
                                }}
                            >
                                <Modal.Header className="app_modal_header" closeButton>
                                    <Modal.Title className="app_modal_header_title">
                                        DELETE PROVIDER
                                    </Modal.Title>
                                </Modal.Header>
                                <Modal.Body className="app_modal_body">
                                    <div>
                                        <p>
                                            Are you sure you want to delete provider "{name}"?
                                        </p>
                                        <OverlayTrigger
                                            trigger={['hover', 'focus']}
                                            placement="bottom"
                                            overlay={
                                                <Tooltip id={`tooltip-bottom`}>
                                                    Confirm Delete   
                                                </Tooltip>
                                            }>
                                            <Button 
                                                variant="danger"
                                                className="pe-7s-trash"
                                                onClick={deleteProvider}
                                            >
                                            </Button>
                                        </OverlayTrigger>
                                    </div>
                                </Modal.Body>
                                {/* <Modal.Footer className="app_modal_footer">
                                    <Button 
                                        className="app_modal_footer_button"
                                        onClick={(e) => {
                                            handleCloseModal(item.name);
                                        }}
                                    >
                                    Close
                                    </Button>
                                </Modal.Footer> */}
                            </Modal>                     
                        </div>
                    </Modal.Body>
                    {/* <Modal.Footer className="app_modal_footer">
                        <Button 
                            className="app_modal_footer_button" 
                            onClick={props.onHide}>
                            Close
                        </Button>
                    </Modal.Footer> */}
                </Modal>
            </div>
        )
    }
    
    return (
        <div>
            {getProviderInfo(props.value)}
        </div>
    );
}

export default ProvidersInfo