import React, { useContext } from "react";

import MathsContext from "../../contexts/MathsContext";

import MathsByGameType from "../Maths/MathsByGameType";

//dictionary to types game and its possible aliases
const aliasMap = {
  BACCARAT: "baccarat",
  BINGO: "bingo",
  LOTTO: "bingo",
  BANCAFRANCESA: "banca_francesa",
  BLACKJACK: "blackjack",
  SLOTS: "spins",
  POKER: "video_poker",
};

const GameMaths = (values) => {
  //maths by method GET from API (Context), for gameslist presentation
  const mathsContext = useContext(MathsContext);
  const { maths } = mathsContext;

  //value of aliases to type's game
  const mathAlias = aliasMap[values.gameType];
  //recognized type's game in the list of maths and verify if it is equals to value of aliases
  const filteredMaths = maths.filter((math) => math.game_type === mathAlias);
  
  return (
    <MathsByGameType
      maths={filteredMaths}
      updateItemGameCasino={values.updateItemGameCasino}
      onClose={values.onClose}
    />
  );
};

export default GameMaths;
