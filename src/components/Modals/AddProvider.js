import React, { useState, useContext } from "react";
import axios from "axios";
import { Form, Col, Button, ButtonGroup, Modal, OverlayTrigger, Tooltip } from "react-bootstrap";

import {AlertSuccess, AlertError} from "../Alerts";

import { ProvidersContext } from "../../contexts/ProvidersContext";

const AddProvider = (props) => {
    //providers by method GET from API (Context)
    const providersContext = useContext(ProvidersContext);
    const { providers, setProviders } = providersContext;

    //variable to store info about new provider addicted
    const [provider, setProvider] = useState(
        {
            name: '',
            url: '',
            username: '',
            password: ''
        }
    );

    //alert messages after API calls
    const [alertMessage, setAlertMessage] = useState('');

    /**
     * function to create a new provider and save data in API
     * @param {*} event 
     */
    const createProvider = (event) => {
        event.preventDefault();
        const object = {
            name: provider.name,
            url: provider.url,
            username: provider.username,
            password: provider.password
        }
        axios.post('/api/providers/', object)
            .then((response) => {
                setProviders([...providers, response.data]);
                props.onHide();
                resetData();
                setAlertMessage("success");
                setTimeout(() => {
                    setAlertMessage("");
                }, 2000);
                
            })
            .catch((err) => { 
                props.onHide();
                resetData();
                setAlertMessage("error");
                setTimeout(() => {
                    setAlertMessage("");
                }, 2000);
                console.log(err);
            }); 
    }
    
    /**
     * function to save data inserted on form and store data on provider variable
     * @param {*} event 
     */
    const onChange = (event) => {
        setProvider(
            {...provider,
                [event.target.name]: event.target.value
            }
        );
    }

    /*
    * Function to reset form data 
    */
    const resetData = () => {
        setProvider({
            name: '',
            url: '',
            username: '',
            password: ''
        });
    }

    return (
        <div>
            {alertMessage === "success" ? <AlertSuccess message={"Provider created successfully"}/> : null}
            {alertMessage === "error" ? <AlertError message={"Error occured while adding the provider."}/> : null}
            <Modal {...props} className="app_modal">
            <Modal.Header className="app_modal_header" closeButton>
                <Modal.Title className="app_modal_header_title">
                    CREATE NEW PROVIDER
                </Modal.Title>
            </Modal.Header>
            <Modal.Body className="app_modal_body">
                    <Form className="new-provider" onSubmit={createProvider}>
                        <Form.Group controlId="formHorizontalName">
                            <Form.Label className="modal_form_label" column sm={2}>
                                Name:
                            </Form.Label>
                            <Col sm={12}>
                                <Form.Control 
                                    className="modal_form_field"
                                    type="text"
                                    name="name"
                                    value={provider.name}
                                    placeholder="Enter the name"
                                    onChange={ onChange }
                                    required
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group controlId="formHorizontalUrl">
                            <Form.Label className="modal_form_label" column sm={2}>
                                URL:
                            </Form.Label>
                            <Col sm={12}>
                                <Form.Control 
                                    className="modal_form_field"
                                    type="text"
                                    name="url"
                                    value={provider.url}
                                    placeholder="Enter the url"
                                    onChange={ onChange }
                                    required
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group controlId="formHorizontalUsername">
                            <Form.Label className="modal_form_label" column sm={2}>
                                Username:
                            </Form.Label>
                            <Col sm={12}>
                                <Form.Control 
                                    className="modal_form_field"
                                    type="text"
                                    name="username"
                                    value={provider.username}
                                    placeholder="Enter the username"
                                    onChange={ onChange }
                                    required
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group controlId="formHorizontalPassword">
                            <Form.Label className="modal_form_label" column sm={2}>
                                Password:
                            </Form.Label>
                            <Col sm={12}>
                                <Form.Control
                                    className="modal_form_field" 
                                    type="text"
                                    name="password"
                                    value={provider.password}
                                    placeholder="Enter the password"
                                    onChange={ onChange }
                                    required
                                />
                            </Col>
                        </Form.Group>
                        <ButtonGroup className="buttons_reset_save">
                            <OverlayTrigger
                                trigger={['hover', 'focus']}
                                placement="bottom"
                                overlay={
                                    <Tooltip id={`tooltip-bottom`}>
                                        Reset   
                                    </Tooltip>
                                }>
                                <Button 
                                    variant="primary" 
                                    className="pe-7s-file"
                                    onClick={resetData}
                                >
                                </Button>
                            </OverlayTrigger>
                            <OverlayTrigger
                                trigger={['hover', 'focus']}
                                placement="bottom"
                                overlay={
                                    <Tooltip id={`tooltip-bottom`}>
                                        Save   
                                    </Tooltip>
                                }>
                                <Button 
                                    variant="success" 
                                    className="pe-7s-diskette"
                                    type="submit"
                                >
                                </Button>
                            </OverlayTrigger>
                        </ButtonGroup>
                    </Form>
                </Modal.Body>
                {/* <Modal.Footer className="app_modal_footer">
                    <Button 
                        className="app_modal_footer_button"
                        onClick={props.onHide}
                    >
                    Close
                    </Button>
                </Modal.Footer> */}
            </Modal>
        </div>
    )
}

export default AddProvider