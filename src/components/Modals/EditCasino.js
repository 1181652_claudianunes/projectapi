import React, { useState } from "react";
import { Form, Col, Row, Button, Modal} from "react-bootstrap";

import {AlertSuccess, AlertError} from "../Alerts";

import { CasinosContext } from "../../contexts//CasinosContext";

const EditCasino = (props) => {
    //casinos by method GET from API (Context)
    const casinosContext = useContext(CasinosContext);
    const { casinos, setCasinos } = casinosContext;

    //variable to store info about casino edited
    const [casino, setCasino] = useState(
        {
            name: props.name ,
            url: props.url,
            username: props.username,
            password: props.password 
        }
    );

    //alert messages after API calls
    const [alertMessage, setAlertMessage] = useState('');

    /**
     * function to edit a casino and save data in API
     * @param {*} e 
     */
    const updateCasino = async (nameCasino) => {
        const object = {
            name: casino.name,
            url: casino.url,
            username: casino.username,
            password: casino.password 
        };
        axios.put('/api/providers/{name_provider}/casinos/' + nameCasino + '/', object)
            .then((response) => {
                const editedCasinos = casinos.map((casino) => {
                    if (casino.name === nameCasino) {
                        casino = object;
                    }
                    return casino;
                });
                setCasinos(editedCasinos);
                props.onHide();
                setAlertMessage("success");
                setTimeout(() => {
                    setAlertMessage("");
                }, 1000);
            })
            .catch((err) => {
                setAlertMessage("error");
                setTimeout(() => {
                    setAlertMessage("");
                }, 1000);
                console.log(err);
            });
        };

    /**
     * function to change data inserted on form and store new data on casino variable
     * @param {*} event 
     */
    const onChange = (event) => {
        setCasino(
            {...casino, 
                [event.target.name]: event.target.value
            }
        );
    }

    return (
        <div>
            {alertMessage === "success" ? <AlertSuccess message={"Casino updated successfully"}/> : null}
            {alertMessage === "error" ? <AlertError message={"Error occured while updating the casino."}/> : null}
            <Modal {...props} className="app_modal">
                <Modal.Header className="app_modal_header" closeButton>
                    <Modal.Title className="app_modal_header_title">
                        EDIT CASINO
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="app_modal_body">
                    <div className="edit-casinos">
                        <Form className="edit-casino" onSubmit={() => updateCasino(props.name)}>
                            <Form.Group as={Row} controlId="formHorizontalName">
                                <Form.Label className="modal_form_label" column sm={2}>
                                    Name:
                                </Form.Label>
                                <Col sm={12}>
                                    <Form.Control 
                                        className="modal_form_field"
                                        type="text"
                                        name="name"
                                        defaultValue={props.name}
                                        placeholder="Enter the name"
                                        disabled
                                        required
                                        onChange={ onChange }
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="formHorizontalUrl">
                                <Form.Label className="modal_form_label" column sm={2}>
                                    URL:
                                </Form.Label>
                                <Col sm={12}>
                                    <Form.Control
                                        className="modal_form_field" 
                                        type="text"
                                        name="url"
                                        defaultValue={props.url}
                                        placeholder="Enter the url"
                                        required
                                        onChange={ onChange }
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="formHorizontalUrl">
                                <Form.Label className="modal_form_label" column sm={2}>
                                    Username:
                                </Form.Label>
                                <Col sm={12}>
                                    <Form.Control
                                        className="modal_form_field" 
                                        type="text"
                                        name="username"
                                        defaultValue={props.username}
                                        placeholder="Enter the username"
                                        required
                                        onChange={ onChange }
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="formHorizontalUrl">
                                <Form.Label className="modal_form_label" column sm={2}>
                                    Password:
                                </Form.Label>
                                <Col sm={12}>
                                    <Form.Control 
                                        className="modal_form_field"
                                        name="password"
                                        defaultValue={props.password}
                                        placeholder="Enter the password"
                                        required
                                        onChange={ onChange }
                                    />
                                </Col>
                            </Form.Group>
                            <OverlayTrigger
                                key="bottom"
                                placement="bottom"
                                overlay={
                                    <Tooltip id={`tooltip-bottom`}>
                                        Save Changes   
                                    </Tooltip>
                                }>
                                <Button 
                                    variant="success" 
                                    className="pe-7s-check"
                                    type="submit"
                                >
                                </Button>
                            </OverlayTrigger>
                        </Form>
                    </div>
                </Modal.Body>
                {/* <Modal.Footer className="app_modal_footer">
                    <Button 
                        className="app_modal_footer_button"
                        onClick={props.onHide}
                    >
                    Close
                    </Button>
                </Modal.Footer> */}
            </Modal>
        </div>
    )
}

export default EditCasino;