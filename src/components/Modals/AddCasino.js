import React, { useState, useContext } from "react";
import axios from "axios";
import { Form, Col, Button, ButtonGroup, Modal, OverlayTrigger, Tooltip } from "react-bootstrap";

import {AlertSuccess, AlertError} from "../Alerts";

import { CasinosContext } from "../../contexts//CasinosContext";

const AddCasino = (props) => {
    //casinos by method GET from API (Context)
    const casinosContext = useContext(CasinosContext);
    const { casinos, setCasinos } = casinosContext;

    //variable to store info about new casino addicted
    const [casino, setCasino] = useState(
        {
            name: '',
            url: '',
            username: '',
            password: ''
        }
    );

    //alert messages after API calls
    const [alertMessage, setAlertMessage] = useState('');

    /**
     * function to create a new casino and save data in API
     * @param {*} event 
     */
    const createCasino = (event) => {
        event.preventDefault();
        const object = {
            name: casino.name,
            url: casino.url,
            username: casino.username,
            password: casino.password
        }
        axios.post('/api/providers/{name_provider}/casinos', object)
            .then((response) => {
                setCasinos([...casinos, response.data]); 
                props.onHide(); 
                resetData();
                setAlertMessage("success");
                setTimeout(() => {
                    setAlertMessage("");
                }, 2000);
               
            })
            .catch((err) => { 
                setAlertMessage("error");
                props.onHide(); 
                setTimeout(() => {
                    setAlertMessage("");
                }, 2000);
                console.log(err);
            }
        ); 
    }

    /**
     * function to save data inserted on form and store data on casino variable
     * @param {*} event 
     */
    const onChange = (event) => {
        setCasino(
            {...casino, 
                [event.target.name]: event.target.value
            }
        );
    }

    /*
    * Function to reset form data 
    */
    const resetData = () => {
        setCasino({
            name: '' ,
            url: '',
            username: '',
            password: '' 
        }); 
    }

    return (
        <div>
            {alertMessage === "success" ? <AlertSuccess message={"Casino created successfully"}/> : null}
            {alertMessage === "error" ? <AlertError message={"Error occured while adding the casino."}/> : null}
            <Modal {...props} className="app_modal">
                <Modal.Header className="app_modal_header" closeButton>
                    <Modal.Title className="app_modal_header_title">
                        CREATE NEW CASINO
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="app_modal_body">
                    <div className="new-casinos">
                        <Form className="new-casino" onSubmit={createCasino}>
                            <Form.Group controlId="formHorizontalName">
                                <Form.Label className="modal_form_label" column sm={2}>
                                    Name:
                                </Form.Label>
                                <Col sm={12}>
                                    <Form.Control 
                                        className="modal_form_field"
                                        type="text"
                                        name="name"
                                        value={casino.name}
                                        placeholder="Enter the name"
                                        onChange={ onChange }
                                        required
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group controlId="formHorizontalUrl">
                                <Form.Label className="modal_form_label" column sm={2}>
                                    URL:
                                </Form.Label>
                                <Col sm={12}>
                                    <Form.Control 
                                        className="modal_form_field"
                                        type="text"
                                        name="url"
                                        value={casino.url}
                                        placeholder="Enter the url"
                                        onChange={ onChange }
                                        required
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group controlId="formHorizontalUsername">
                                <Form.Label className="modal_form_label" column sm={2}>
                                    Username:
                                </Form.Label>
                                <Col sm={12}>
                                    <Form.Control 
                                        className="modal_form_field"
                                        type="text"
                                        name="username"
                                        value={casino.username}
                                        placeholder="Enter the username"
                                        onChange={ onChange }
                                        required
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group controlId="formHorizontalPassword">
                                <Form.Label className="modal_form_label" column sm={2}>
                                    Password:
                                </Form.Label>
                                <Col sm={12}>
                                    <Form.Control
                                        className="modal_form_field" 
                                        type="text"
                                        name="password"
                                        value={casino.password}
                                        placeholder="Enter the password"
                                        onChange={ onChange }
                                        required
                                    />
                                </Col>
                            </Form.Group>
                            <ButtonGroup aria-label="Basic example" className="buttons_reset_save">
                                <OverlayTrigger
                                    key="bottom"
                                    placement="bottom"
                                    overlay={
                                        <Tooltip id={`tooltip-bottom`}>
                                            Reset   
                                        </Tooltip>
                                    }>
                                    <Button 
                                    variant="primary" 
                                    className="pe-7s-file"
                                    onClick={resetData}
                                    >
                                    </Button>
                                </OverlayTrigger>
                                <OverlayTrigger
                                    key="bottom"
                                    placement="bottom"
                                    overlay={
                                        <Tooltip id={`tooltip-bottom`}>
                                            Save   
                                        </Tooltip>
                                    }>
                                    <Button 
                                    variant="success" 
                                    className="pe-7s-diskette"
                                    type="submit"
                                    >
                                    </Button>
                                </OverlayTrigger>
                            </ButtonGroup>
                        </Form>
                    </div>
                </Modal.Body>
                {/* <Modal.Footer className="app_modal_footer">
                    <Button 
                        className="app_modal_footer_button"
                        onClick={props.onHide}
                    >
                    Close
                    </Button>
                </Modal.Footer> */}
            </Modal>
        </div>
    )
}

export default AddCasino