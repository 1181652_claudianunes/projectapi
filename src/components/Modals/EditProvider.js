import React, { useState, useContext } from "react";
import axios from 'axios';
import { Form, Col, Row, Button, Modal, OverlayTrigger, Tooltip} from "react-bootstrap";

import {AlertSuccess, AlertError} from "../Alerts";

import { ProvidersContext } from "../../contexts//ProvidersContext";

const EditProvider = (props) => {
    //providers by method GET from API (Context)
    const providersContext = useContext(ProvidersContext);
    const { providers, setProviders } = providersContext;
    
    //variable to store info about provider edited
    const [provider, setProvider] = useState(
        {
            id: props.id,
            name: props.name,
            url: props.url,
            username: props.username,
            password: props.password
        }
    );
    
    //alert messages after API calls
    const [alertMessage, setAlertMessage] = useState('');
    
    /**
     * function to edit a provider and save data in API
     * @param {name} event 
     */

    //API - METHOD PUT USING AXIOS 
    const updateProvider = async (nameProvider) => {
        const object = {
            name: provider.name,
            url: provider.url,
            username: provider.username,
            password: provider.password
        };
        axios.put('/api/providers/' + nameProvider + '/', object)
            .then(res => {
                const editedProviders = providers.map((provider) => {
                    if (provider.name === nameProvider) {
                        provider = object;
                    }
                    return provider;
                });
                setProviders(editedProviders);
                console.log(res);
                props.onHide();
                setAlertMessage("success");
                setTimeout(() => {
                    setAlertMessage("");
                }, 2000);
            })
            .catch(error => {
                props.onHide();
                setAlertMessage("error");
                setTimeout(() => {
                    setAlertMessage("");
                }, 2000); 
            });
        };

    /**
     * function to change data inserted on form and store new data on provider variable
     * @param {*} event 
     */
    const onChange = (event) => {
        setProvider(
            {...provider, 
                [event.target.name]: event.target.value
            }
        );
    }

    return (
        <div>
            {alertMessage === "success" ? <AlertSuccess message={"Provider updated successfully"}/> : null}
            {alertMessage === "error" ? <AlertError message={"Error occured while updating the provider."}/> : null}
            <Modal {...props} className="app_modal">
                <Modal.Header className="app_modal_header" closeButton>
                    <Modal.Title className="app_modal_header_title">
                        EDIT PROVIDER
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="app_modal_body">
                    <div className="edit-providers">
                        <Form className="edit-provider">
                            <Form.Group as={Row}>
                                <Form.Label className="modal_form_label" column sm={2}>
                                    Name:
                                </Form.Label>
                                <Col sm={12}>
                                    <Form.Control 
                                        className="modal_form_field"
                                        type="text"
                                        name="name"
                                        defaultValue={props.name}
                                        placeholder="Enter the name"
                                        disabled
                                        required
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="formHorizontalUrl">
                                <Form.Label className="modal_form_label" column sm={2}>
                                    URL:
                                </Form.Label>
                                <Col sm={12}>
                                    <Form.Control 
                                        className="modal_form_field"
                                        type="text"
                                        name="url"
                                        defaultValue={props.url}
                                        placeholder="Enter the url"
                                        required
                                        onChange={ onChange }
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="formHorizontalUsername">
                                <Form.Label className="modal_form_label" column sm={2}>
                                    Username:
                                </Form.Label>
                                <Col sm={12}>
                                    <Form.Control 
                                        className="modal_form_field"
                                        type="text"
                                        name="username"
                                        defaultValue={props.username}
                                        placeholder="Enter the username"
                                        required
                                        onChange={ onChange }
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="formHorizontalPassword">
                                <Form.Label className="modal_form_label" column sm={2}>
                                    Password:
                                </Form.Label>
                                <Col sm={12}>
                                    <Form.Control 
                                        className="modal_form_field"
                                        name="password"
                                        defaultValue={props.password}
                                        placeholder="Enter the password"
                                        required
                                        onChange={ onChange }
                                    />
                                </Col>
                            </Form.Group>
                            <OverlayTrigger
                                key="bottom"
                                placement="bottom"
                                overlay={
                                    <Tooltip id={`tooltip-bottom`}>
                                        Save Changes   
                                    </Tooltip>
                                }>
                                <Button 
                                    variant="success" 
                                    className="pe-7s-check"
                                    onClick={() => updateProvider(props.name)}
                                >
                                </Button>
                            </OverlayTrigger>
                        </Form>
                    </div>
                </Modal.Body>
                {/* <Modal.Footer className="app_modal_footer">
                    <Button 
                        className="app_modal_footer_button"
                        onClick={props.onHide}
                    >
                    Close
                    </Button>
                </Modal.Footer> */}
            </Modal>
        </div>
    )
}

export default EditProvider;