import React, { useState, useEffect } from "react";
import { InputGroup, FormControl, Button, Dropdown, DropdownButton, OverlayTrigger, Popover, Tooltip } from "react-bootstrap";

const MathsByGameType = ({ maths, updateItemGameCasino, onClose }) => {
  //maths selected and filtered by search bar
  const [expandedMathsInfo, setExpandedMathsInfo] = useState([]);
  const [searchMaths, setSearchMaths] = useState("");
  const [filteredMaths, setFilteredMaths] = useState([]);

  //Dropdown to filter maths
  const [dropDownValue, setDropDownValue] = useState("Search By");

  //Button to filter maths by state (debug, release or all)
  const [arrayStates] = useState([
    {
      idType: "0",
      type: "All Maths",
    },
    {
      idType: "1",
      type: "Maths Release",
    },
    {
      idType: "2",
      type: "Maths Debug",
    },
  ]);
  //Atual state to button (all maths)
  const [atualState, setAtualState] = useState('0');

  //Checkbox to select a math for a game
  const [isSelected, setIsSelected] = useState(false);

  /*
   * Function to change the name of button that shows results of maths (all, release or debug)
   */
  const changeState = () => {
    var changeNext = false;
    var hasChanged = false;
    arrayStates.forEach((item) => {
      // verify if it is to change atual state for new item
      if (changeNext && !hasChanged) {
        setAtualState(item.idType);
        hasChanged = true;
      }
      // verify atual position in the array
      if (item.idType === atualState) {
        changeNext = true;
      }
    });
    // If atual state is on the last position of the array, it actualiaze to "0" (ALL MATHS)
    if (changeNext && !hasChanged) {
      setAtualState('0');
    }
  };

  /*
   * Function to expand info about math
   */
  const handleClickMathInfo = (itemMath) => {
    if (expandedMathsInfo.includes(itemMath)) {
      const filterMathsInfo = expandedMathsInfo.filter(
        (expandedMathInfo) => expandedMathInfo !== itemMath
      );
      setExpandedMathsInfo(filterMathsInfo);
    } else {
      setExpandedMathsInfo([itemMath, ...expandedMathsInfo]);
    }
  };

  /*
   * Function to change the value of Dropdown list, to filter maths
   */
  const changeValue = (text) => {
    setDropDownValue(text);
  };

  /*
   * Function to add version of a game and store that value on object "itemGameCasino"
   */
  const addGameMathToItemGameCasino = (mathGameCasino) => {
        updateItemGameCasino({ math: mathGameCasino });
  };

  /*
   * Function to select checkbox item and show maths modal associated
   */
  const handleCheckBoxVersion = (itemMath) => {
    setIsSelected(!isSelected);
    if (!isSelected) {
      addGameMathToItemGameCasino(itemMath);
      onClose();
    }
  };

  /*
   * Function to filter maths by search bar
   * Function to show results by state button
   */
  useEffect(() => {
    switch (dropDownValue) {
      case "Description":
        setFilteredMaths(
          maths.filter((math) => {
            return (
              math.description
                .toLowerCase()
                .includes(searchMaths.toLowerCase()) &&
              (atualState === '0' ||
                (atualState === '1' && !math.is_debug) ||
                (atualState === '2' && math.is_debug))
            );
          })
        );
        break;
      case "Protocol Version":
        setFilteredMaths(
          maths.filter((math) => {
            return (
              math.protocol_version
                .toLowerCase()
                .includes(searchMaths.toLowerCase()) &&
              (atualState === '0' ||
                (atualState === '1' && !math.is_debug) ||
                (atualState === '2' && math.is_debug))
            );
          })
        );
        break;
      default:
        setFilteredMaths(
          maths.filter((math) => {
            return (
              math.description
                .toLowerCase()
                .includes(searchMaths.toLowerCase()) &&
              (atualState === '0' ||
                (atualState === '1' && !math.is_debug) ||
                (atualState === '2' && math.is_debug))
            );
          })
        );
        break;
    }
  }, [searchMaths, atualState, maths, dropDownValue]);

  /*
   * Function to get the description of the math
   */
  const getMathDescription = (math) => {
    return (
      <div key={math.description}>
        
          <InputGroup className="mb-3">
            <DropdownButton
              as={InputGroup.Prepend}
              id="dropdown-item-button"
              className="format"
              title={dropDownValue}
            >
              <Dropdown.Item>
                <div onClick={(e) => changeValue(e.target.textContent)}>
                  Description
                </div>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item>
                <div onClick={(e) => changeValue(e.target.textContent)}>
                  Protocol Version
                </div>
              </Dropdown.Item>
            </DropdownButton>
            <FormControl
              aria-describedby="basic-addon1"
              onChange={(e) => setSearchMaths(e.target.value)}
            />
          </InputGroup>
        
          <Button
            className="filter_button"
            onClick={() => changeState()}
            block
          >
            {
              arrayStates.filter((e) => {
                return e.idType === atualState;
              })[0].type
            }
          </Button>
        
        <hr />
        <div>
          {filteredMaths
            .sort((a, b) => (a.description > b.description ? 1 : -1))
            .map((mathItem) => {
              return (
                <div key={mathItem.description}>
                  <InputGroup className="info_elements_list">
                    <InputGroup.Prepend>
                      <OverlayTrigger
                        trigger={['hover', 'focus']}
                        key="left"
                        placement="left"
                        overlay={
                          <Tooltip id={`tooltip-left`}>
                            Select Math   
                          </Tooltip>
                        }>
                        <Button 
                          className="info_elements_btn_checkbox"
                          onClick={(e) => handleCheckBoxVersion(mathItem.description)}
                        >
                        </Button>
                      </OverlayTrigger>
                    </InputGroup.Prepend>
                    <OverlayTrigger
                        trigger={['hover', 'focus']}
                        key="right"
                        placement="right"
                        overlay={
                          <Popover id={`popover-positioned-right`}>
                            <Popover.Content>
                              <div>
                                  <strong>Protocol Version:</strong>{' '}{mathItem.protocol_version}
                                  <br />
                                  <strong>Debug:</strong>{' '}{String(mathItem.is_debug)}
                              </div>
                            </Popover.Content>
                          </Popover>
                        }>
                      <Button 
                        className="info_elements_btn"
                        onClick={() => handleClickMathInfo(mathItem.description)}
                        block
                      >{mathItem.description}
                      </Button>
                    </OverlayTrigger>
                  </InputGroup>
                </div>
              );
            })}
        </div>
      </div>
    );
  };

  //returns the description of the math obtained by function "getMathDescription"
  return (
    <div>
      {getMathDescription(maths)}
    </div>
  );
};

export default MathsByGameType;
