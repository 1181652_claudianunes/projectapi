import React from "react";
import { Alert } from "react-bootstrap";

export const AlertSuccess = (props) => {
  return (
    <div>
      <Alert variant="success">
        {props.message}
      </Alert>
    </div>
  )
}

export const AlertError = (props) => {
  return (
    <div>
      <Alert variant="danger">
        {props.message}
      </Alert>
    </div>  
  )
}

export default { AlertError, AlertSuccess };
