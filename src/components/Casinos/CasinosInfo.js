import React, { useState } from "react";
import { Modal, ButtonGroup, Button, OverlayTrigger, Tooltip } from "react-bootstrap";

import { CasinosContext } from "../../contexts/CasinosContext";

import EditCasino from "../Modals/EditCasino";

const CasinosInfo = ({value}) => {
    //casinos by method GET from API (Context)
    const casinosContext = useContext(CasinosContext);
    const { casinos, setCasinos } = casinosContext;

    //modal to edit casino and its provider fields (name, ...)
    const [editModalShow, setEditModalShow] = useState(false);
    const [name, setName] = useState();
    const [url, setUrl] = useState();
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
        
    //modal to delete casino
    const [deleteModalShow, setDeleteModalShow] = useState(false);
  
    /*
    * Function to open the modal 
    */
    const handleShowModal = () => {
        setDeleteModalShow(true);
    }

    /*
    * Function to close the modal 
    */
    const handleCloseModal = (itemCasino) => {
        setDeleteModalShow(false);
    }

    const getCasinoInfo = (item) => {
        /*
        * Function to delete a provider
        */ 
        //API - METHOD DELETE USING AXIOS
        const deleteCasino = (() => {
            axios.delete(`/api/providers/{nameProvider}/${name}`)
                .then(response => {
                    const deletedCasinos = casinos.filter((casino) => casino.name !== name);
                    setCasinos(deletedCasinos); 
                    props.onPreHide("success")
                    props.onHide();
                })
                .catch((err) => { 
                    props.onPreHide("error")
                    props.onHide();
                }); 
        })
        /*
        * Function to edit a casino
        */ 
        const editCasino = (name) => {
            setEditModalShow(true)
            setName(item.name)
            setUrl(item.url)
            setUsername(item.username)
            setPassword(item.password)
        }

        return (
            <div>
                <Modal {...props}  className="app_modal">
                    <Modal.Header className="app_modal_header" closeButton>
                        <Modal.Title className="app_modal_header_title">
                            {name.toUpperCase()}
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body className="app_modal_body">
                        <EditCasino 
                            show={editModalShow}
                            onHide={() => {
                                setEditModalShow(false);
                            }}
                            id={item.id}
                            name={name}
                            url={url}
                            username={username}
                            password={password}
                        /> 
                        <div className="info_elements">
                            <h5 className="info_elements_title">URL:</h5>
                            <p className="info_elements_body">"{item.url}"</p>
                        </div>
                        <div className="info_elements">
                            <h5 className="info_elements_title">Username:</h5>
                            <p className="info_elements_body">"{item.username}"</p>
                        </div>
                        <div className="info_elements">
                            <h5 className="info_elements_title">Password:</h5>
                            <p className="info_elements_body">"{item.password}"</p>
                        </div>
                        <div>
                            <ButtonGroup aria-label="Basic example" className="buttons_edit_delete">
                                <OverlayTrigger
                                    placement="bottom"
                                    overlay={
                                        <Tooltip id={`tooltip-bottom`}>
                                            Edit   
                                        </Tooltip>
                                    }>
                                    <Button 
                                        variant="warning"
                                        className="pe-7s-pen"
                                        onClick={() => editCasino(item.name)}
                                    >
                                    </Button>
                                </OverlayTrigger>
                                <br />
                                <OverlayTrigger
                                    trigger={['hover', 'focus']}
                                    placement="bottom"
                                    overlay={
                                        <Tooltip id={`tooltip-bottom`}>
                                            Delete   
                                        </Tooltip>
                                    }>
                                    <Button 
                                        variant="danger"
                                        className="pe-7s-trash"
                                        onClick={handleShowModal}
                                    >
                                    </Button>
                                </OverlayTrigger>
                            </ButtonGroup>   
                            <Modal 
                                className="app_modal"
                                show={deleteModalShow} 
                                onHide={(e) => {
                                    handleCloseModal(item.name);
                                }}
                            >
                                <Modal.Header className="app_modal_header" closeButton>
                                    <Modal.Title className="app_modal_header_title">
                                        DELETE CASINO
                                    </Modal.Title>
                                </Modal.Header>
                                <Modal.Body className="app_modal_body">
                                    <div>
                                        <p>
                                            Are you sure you want to delete casino "{name}"?
                                        </p>
                                        <OverlayTrigger
                                            trigger={['hover', 'focus']}
                                            placement="bottom"
                                            overlay={
                                                <Tooltip id={`tooltip-bottom`}>
                                                    Confirm Delete   
                                                </Tooltip>
                                            }>
                                            <Button 
                                                variant="danger"
                                                className="pe-7s-trash"
                                                onClick={deleteProvider}
                                            >
                                            </Button>
                                        </OverlayTrigger>
                                    </div>
                                </Modal.Body>
                                {/* <Modal.Footer className="app_modal_footer">
                                    <Button 
                                        className="app_modal_footer_button"
                                        onClick={(e) => {
                                            handleCloseModal(item.name);
                                        }}
                                    >
                                    Close
                                    </Button>
                                </Modal.Footer> */}
                            </Modal>                     
                        </div>
                    </Modal.Body>
                    <Modal.Footer className="app_modal_footer">
                        <Button 
                            className="app_modal_footer_button" 
                            onClick={props.onHide}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
    
    return (
        <div>
            {getCasinoInfo(value)}
        </div>
    );
}

export default CasinosInfo