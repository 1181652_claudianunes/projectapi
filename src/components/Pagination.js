import React from "react";

const PaginationList = ({ itemsPerPage, totalItems, currentPage, setCurrentPage }) => {   
    const paginate = (pageNum) => {
        setCurrentPage(pageNum)
    }
       
    const nextPage = () => {
        setCurrentPage(currentPage + 1)
    }

    const prevPage = () => {
        setCurrentPage(currentPage - 1)
    }

    // valid and navigable address as the href value 
    let hrefLink = '#';

    //calculate page number by data API items 
    const pageNumbers = [];
    const maxPages = Math.ceil(totalItems / itemsPerPage); 
    for (let num = 1; num <= maxPages; num++) {
        pageNumbers.push(num);
    }
    
    //if no data API, return null
    if (totalItems === 0 ) return null;

    return (
        <nav>
            <ul className="pagination justify-content-center">
                <li className={(currentPage <= 1 ? "page-item disabled" : "page-item")}>
                    <a className="page-link" href={hrefLink} onClick={() => prevPage()}>
                        Previous
                    </a>
                </li>
                {pageNumbers.map(num => (
                    <li className={(currentPage === num ? "page-item active" : "page-item")}  key={num} >
                        <a className="page-link" href={hrefLink} onClick={() => paginate(num)}>{num}</a>
                    </li>
                ))}
                <li className={(currentPage >= maxPages ? "page-item disabled" : "page-item")}>
                    <a className="page-link" href={hrefLink} onClick={() => nextPage()}>
                        Next
                    </a>
                </li>
            </ul>
        </nav>
    )  
}

export default PaginationList

