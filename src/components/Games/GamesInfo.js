import React, { useState, useEffect } from "react";
import { Button, InputGroup, OverlayTrigger, Popover, Tooltip } from "react-bootstrap";

const GamesInfo = ({ value, updateItemGameCasino, onClose }) => {
  //Versions of the selected game
  const versions = value.versions;
  const [expandedVersions, setExpandedVersions] = useState([]);

  //Versions filtered by status
  const [filteredVersions, setFilteredVersions] = useState([]);

  //Button to select a version for a game
  const [isSelected, setIsSelected] = useState(false);

  
  /**
   * itemVersion is the DOM element that triggered the OnClick event.
   */
  const handleClickVersion = (itemVersion) => {
    if (expandedVersions.includes(itemVersion)) {
      const filterVersions = expandedVersions.filter(
        (expandedVersion) => expandedVersion !== itemVersion
      );
      setExpandedVersions(filterVersions);
    } else {
      setExpandedVersions([itemVersion, ...expandedVersions]);
    }
  };

  /*
  * Function to filter versions by status 
  * if (status === "CLOSE" || status === "GOLD" || status === "RC") -> show version in list 
  * if (status === "NEW" || status === "BUILDING" || status === "REVIEW" || status === "REOPEN") -> ignored and not show version in list 
  */
  useEffect(() => {
    if (typeof versions !== "undefined" && versions !== null)  {
      const status = ["CLOSED","GOLD","RC"];
      let versionsStatus = [];
      status.forEach((status) => {
        versionsStatus = versionsStatus.concat(versions.filter((item) => {
          // only search by versions that haven't yet been filtered by status
          return (item.status === status && (versionsStatus.filter(item2 => {
            return (item2.version === item.version)
          }).length === 0))
        }));
      });
      setFilteredVersions(versionsStatus);   
    }
  }, [versions]); 

  /*
   * Function to add version of a game and store that value on object "itemGameCasino"
   */
  const addGameVersionToItemGameCasino = (versionGameCasino) => {
    updateItemGameCasino({ version: versionGameCasino });
  };

  /*
   * Function to select checkbox item and show maths modal associated
   */
  const handleCheckBoxVersion = (itemVersion) => {
    setIsSelected(!isSelected);
    if (!isSelected) {
      addGameVersionToItemGameCasino(itemVersion);
      onClose();
    }
  };

  /*
   * Function to get the info of the game
   */
  const getGameInfo = (item) => {
    return (
      <div key={item.game_name}>
        <div className="info_elements">
          <h5 className="info_elements_title">Type:</h5>
          <p className="info_elements_body">"{item.game_type}"</p>
        </div>
        <div>
          <h5 className="info_elements_title">Versions:</h5>
          {filteredVersions.map((version,id) => {
            return (
              <div key={id}>
                <InputGroup className="info_elements_list">
                  <InputGroup.Prepend>
                    <OverlayTrigger
                      trigger={['hover', 'focus']}
                      key="left"
                      placement="left"
                      overlay={
                        <Tooltip id={`tooltip-left`}>
                          Select Version   
                        </Tooltip>
                      }>
                      <Button 
                        className="info_elements_btn_checkbox"
                        onClick={(e) => handleCheckBoxVersion(version.id)}
                      >
                      </Button>
                    </OverlayTrigger>
                  </InputGroup.Prepend>
                    <OverlayTrigger
                      trigger={['hover', 'focus']}
                      key="right"
                      placement="right"
                      overlay={
                        <Popover id={`popover-positioned-right`}>
                          <Popover.Content>
                            <div>
                              <strong>Status:</strong>{' '}{version.status}
                              <br />
                              <strong>Modified Data:</strong>{' '}{version.modified}
                            </div>
                          </Popover.Content>
                        </Popover>
                      }>
                      <Button 
                        className="info_elements_btn"
                        onClick={() => handleClickVersion(version.version)}
                        block
                      >{version.version}_{version.status}
                      </Button>
                    </OverlayTrigger>
                </InputGroup>
              </div>
            );
          })}
        </div>
      </div>
    );
  };

  //returns the value (info game) obtained by function "getGameInfo()"
  return (
    <div>
      {getGameInfo(value)}
    </div>
  );

};

export default GamesInfo;
