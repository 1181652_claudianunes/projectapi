import React from "react";

import HomePage from "./pages/Home/HomePage";
import GamesList from "./pages/Games/GamesList";
import MathsList from "./pages/Maths/MathsList";
import ProvidersList from "./pages/Providers/ProvidersList";
import CasinosList from "./pages/Casinos/CasinosList";

import { GamesProvider } from "./contexts/GamesContext";
import { MathsProvider } from "./contexts/MathsContext";
import { ProvidersProvider } from "./contexts/ProvidersContext";
import { CasinosProvider } from "./contexts/CasinosContext";

const dashboardRoutes = [
  {
    path: "/",
    name: "Dashboard",
    icon: "pe-7s-menu",
    component: HomePage,
    providers: ['games', 'maths', 'providers', 'casinos']
  },
  {
    path: "/games",
    name: "Games",
    icon: "pe-7s-joy",
    component: GamesList,
    providers: ['games', 'maths']
  },
  {
    path: "/maths",
    name: "Maths",
    icon: "pe-7s-calculator",
    component: MathsList,
    providers: ['maths']
  },
  {
    path: "/providers",
    name: "Providers",
    icon: "pe-7s-server",
    component: ProvidersList,
    providers: ['providers', 'casinos']
  },
  {
    path: "/casinos",
    name: "Casinos",
    icon: "pe-7s-user",
    component: CasinosList,
    providers: ['providers', 'casinos']
  }
];
export default dashboardRoutes;


// https://kyleshevlin.com/how-to-dynamically-render-react-components
export const BlockComponent = ((type, key, children) => {
  switch (type) {
    case 'games':
      return (<GamesProvider key={key}>{children}</GamesProvider>)

    case 'maths':
      return <MathsProvider key={key}>{children}</MathsProvider>

    case 'providers':
      return <ProvidersProvider key={key}>{children}</ProvidersProvider>

    case 'casinos':
      return <CasinosProvider key={key}>{children}</CasinosProvider>

    default:
      return <div className="no_block_type" />
  }
});
