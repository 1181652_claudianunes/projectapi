import React, { Component } from "react";
import { Route } from "react-router-dom";

import  Sidebar  from "../components/SideBar/SideBar";

import routes, { BlockComponent } from "../routes";


class Admin extends Component {
    constructor(props) {
      super(props);
      this.state = {
        color: "black"
      };
    }
   
    getRoutes = routes => {
      return routes.map((prop, key) => {
        if (this.props.location.pathname === prop.path){
          return this.getProviders(prop, key, 0);
        }else{
          return null;
        }

      });
    };

    getProviders = (prop, key, index) => {
      if (prop.providers.length > 0 && prop.providers.length !== index){
        const prov = this.getProviders(prop, key, (index+1))         
        return BlockComponent(prop.providers[index], key, prov)
      }else{
        return (
          <Route exact path={prop.path}>
            <prop.component/>
          </Route>
        );
      }

    }

    componentDidMount() {

    }
    
    componentDidUpdate(e) {
      if (
        window.innerWidth < 993 &&
        e.history.location.pathname !== e.location.pathname &&
        document.documentElement.className.indexOf("nav-open") !== -1
      ) {
        document.documentElement.classList.toggle("nav-open");
      }
      if (e.history.action === "PUSH") {
        document.documentElement.scrollTop = 0;
        document.scrollingElement.scrollTop = 0;
        this.refs.mainPanel.scrollTop = 0;
      }
    }
    
    render() {
      return (
        <div className="wrapper">
          <Sidebar {...this.props} routes={routes}/>
          <div id="main-panel" className="main-panel" ref="mainPanel">
            {this.getRoutes(routes)}
          </div>
        </div>
      );
    }
  }
  
  export default Admin;
  