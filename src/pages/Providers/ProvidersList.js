import React, { useContext, useState, useEffect } from "react";
import { Container, Row, Col, Button, InputGroup, FormControl, Modal, Spinner, OverlayTrigger, Tooltip } from "react-bootstrap";

import { ProvidersContext } from "../../contexts/ProvidersContext";

import {AlertSuccess, AlertError} from "../../components/Alerts";

import PaginationList from "../../components/Pagination";
import ProvidersInfo from "../../components/Providers/ProvidersInfo.js";
import AddProvider from "../../components/Modals/AddProvider";

const ProvidersList = ( props ) => {
    //providers by method GET from API (Context)
    const providersContext = useContext(ProvidersContext);
    const { providers, loading } = providersContext;

    //provider selected and filtered by search bar 
    const [expandedProviders, setExpandedProviders] = useState([]);
    const [searchProviders, setSearchProviders] = useState("");
    const [filteredProviders, setFilteredProviders] = useState([]);
    
    //modal to show info about  selected provider
    const [showModal, setShowModal] = useState(false);

    //modal to add new provider
    const [addModalShow, setAddModalShow] = useState(false);
    
    //pagination
    const [currentPage, setCurrentPage] = useState(1);
    const [providersPerPage] = useState(18);

    //alert messages after API calls
    const [alertStatus, setAlertStatus] = useState('');
    const [alertMessage, setAlertMessage] = useState('');


    /*
    * Function to open the modal
    */
    const handleShowModal = () => {
        setShowModal(true);
    }
    /*
    * Function to close the modal 
    */
    const handleCloseModal = (itemProvider, alerta) => {
        handleClickInfo(itemProvider);
        setShowModal(false);

        if (alerta !== undefined){
            if(alerta !== ""){
                
            }
        }
    }

    /** 
     * Function to filter and show provider when it is clicked
     * itemProvider is the DOM element that triggered the OnClick event.
    */
    const handleClickInfo = (itemProvider) => {
        //if game clicked is included on expandedGames, filterGames delete repeat item
        if (expandedProviders.includes(itemProvider)){
            const filterProviders = expandedProviders.filter(
                (expandedProvider) => expandedProvider !== itemProvider
            );
            setExpandedProviders(filterProviders);
        }else {
            setExpandedProviders([itemProvider, ...expandedProviders]);
        }
    } 
    
    /*
    * Function to filter providers by search bar 
    */
    useEffect(() => {
        if (typeof providers !== "undefined" && providers !== null)  {
            setFilteredProviders(
                providers.filter((provider) => {
                    return provider.name.toLowerCase().includes(searchProviders.toLowerCase())
                })
            );
        }
    }, [searchProviders, providers]);

    //RETURNS PROVIDERS LIST 
    return (
        <>
            <Container className="page_container">
                <Row className="page_row">
                    <Col className="page_col_top">
                        <div className="page_head">
                            <h1 className="page_head_title">  
                                Providers
                            </h1>
                        </div>
                        <InputGroup className="mb-4">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="basic-addon1">
                                    <i className="pe-7s-search"></i>
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                                aria-label="Username"
                                aria-describedby="basic-addon1"
                                onChange={ e => {setSearchProviders(e.target.value); setCurrentPage("1")}}
                            />
                        </InputGroup>
                        <OverlayTrigger
                            trigger={['hover', 'focus']}
                            key="left"
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-"left"`}>
                                 <strong>Create New Provider</strong>
                                </Tooltip>
                            }
                        >
                        <div className="page_popup">
                            <div className="page_show_popup">
                                <div className="page_show_popup_div">
                                <i className="pe-7s-plus"
                                    onClick={() => setAddModalShow(true)}
                                >
                                </i>
                                </div>
                            </div>
                        </div>
                        </OverlayTrigger>

                        {alertStatus === "success" ? <AlertSuccess message={alertMessage}/> : null}
                        {alertStatus === "error" ? <AlertError message={alertMessage}/> : null}
                
                        <AddProvider 
                            show={addModalShow}
                            onHide={(e) => {
                                setAddModalShow(false);
                            }}
                        />
                {loading ? (
                    <Spinner animation="border" variant="primary">
                        <span className="sr-only">Loading...</span>
                    </Spinner>
                ) : (
                    <Row className="page_row">
                        {filteredProviders
                            .sort((a, b) => a.name > b.name ? 1 : -1)
                            .slice(
                                (currentPage-1) * providersPerPage,
                                currentPage * providersPerPage
                                )
                            .map((provider) => {
                                return (
                                    <Col lg={2} md={3} xs={12} className="page_col_list" key={provider.name}>
                                        <div
                                            className="page_list_div"
                                            onClick={() => {
                                                handleClickInfo(provider.name);
                                                handleShowModal();
                                            }}
                                        >
                                            {provider.name}
                                        </div>
                                        {
                                            expandedProviders.includes(provider.name) && 
                                            <ProvidersInfo
                                                value={provider}
                                                show={showModal}
                                                onPreHide={(status) => {
                                                    if (status !== ""){
                                                        setAlertStatus(status);
                                                        if (status === "success"){
                                                            setAlertMessage("Provider deleted successfully");
                                                        }else{
                                                            setAlertMessage("Error occured while deleting the provider");
                                                        }
                                                        setTimeout(() => {
                                                            setAlertStatus("");
                                                            setAlertMessage("");
                                                        }, 2000);
                                                    }
                                                }}
                                                onHide={(status) => {
                                                    handleCloseModal(provider.name);
                                                }}

                                            />

                                        }
                                    </Col>
                                ); 
                            })}
                    </Row>
                )} 
                <PaginationList 
                    itemsPerPage={providersPerPage} 
                    totalItems={filteredProviders.length}  
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                />
                    </Col>
                </Row>
            </Container>
        </>
    )
}   

export default ProvidersList;