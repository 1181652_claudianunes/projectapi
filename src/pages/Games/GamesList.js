import React, { useContext, useState, useEffect } from "react";
import axios from "axios";
import { Container, Row, Col, Button, InputGroup, FormControl, Modal, Table, Spinner, OverlayTrigger, Tooltip } from "react-bootstrap";


import { GamesContext } from "../../contexts/GamesContext";

import PaginationList from "../../components/Pagination";
import GamesInfo from "../../components/Games/GamesInfo";
import GameMaths from "../../components/Modals/GameMaths";


//object itemGameCasino empty, to be possible to fill with another game to add to gamesCasino array
const initialItemGameCasino = {
  game: "",
  version: "",
  math: "",
};

//object defaultConfigsGame empty, to be possible to fill with info about default configs of game selected
const initialDefaultConfigGame = {
  Game: {
    name: "",
    ids: {
      name: "",
      basename: ""
    },
    jackpot_enabled: false,
    type: "",
    type_descr: ""
  },
  Casino: {
    name: ""
  },
  GameResolution: {
    width: null,
    height: null
  },
  GameSpec: {
    math_paths: [],
    max_bet: null,
    min_bet: null,
    chip_values: ""
  },
  CurrencyDenominations: {
    default: {
      denominations: null,
      default_denomination: null
    }
  }
}

export const GamesList = (game) => {
  //games by method GET from API (Context), for gameslist presentation
  const gamesContext = useContext(GamesContext);
  const { games, loading } = gamesContext;

  //games selected and filtered by search bar
  const [expandedGames, setExpandedGames] = useState([]);
  const [searchGames, setSearchGames] = useState("");
  const [filteredGames, setFilteredGames] = useState([]);

  //pagination
  const [currentPage, setCurrentPage] = useState(1);
  const [gamesPerPage] = useState(18);

  //modal to show info about selected game
  const [showModal, setShowModal] = useState(false);

  //modal to show table to add games to casino
  const [showModalTable, setShowModalTable] = useState(false);

  //modal to show math by type game
  const [mathModalShow, setMathModalShow] = useState(false);

  //modal to show game info
  const [gameModalShow, setGameModalShow] = useState(true);

  //game object array to store info about game that will be add to certain casino
  const [gamesCasino, setGamesCasino] = useState([]);

  //game object to store info gamesCasino array
  const [itemGameCasino, setItemGameCasino] = useState(initialItemGameCasino);

  //default configs of game that will be install on casino
  const [defaultConfigsGame, setDefaultConfigsGame] = useState(initialDefaultConfigGame);

  /*
   * Function to open the modal
   */
  const handleShowModal = () => {
    setShowModal(true);
  };

  /*
   * Function to close the modal
   */
  const handleCloseModal = (itemGame) => {
    handleClickInfo(itemGame);
    setShowModal(false);
    setGameModalShow(true);
    setMathModalShow(false);
  };

  /**
   * Function to filter and show game when it is clicked
   * itemGame is the DOM element that triggered the OnClick event.
   */
  const handleClickInfo = (itemGame) => {
    //if game clicked is included on expandedGames, filterGames delete repeat item
    if (expandedGames.includes(itemGame)) {
      const filterGames = expandedGames.filter(
        (expandedGame) => expandedGame !== itemGame
      );
      setExpandedGames(filterGames);
    } else {
      setExpandedGames([itemGame, ...expandedGames]);
    }
  };

  /*
   * Function to update itemGameCasino when it received info by other components
   * and store itemGameCasino (game, version + math) on array gamesCasino
   */
  const updateItemGameCasino = (data) => {
    setItemGameCasino({ ...itemGameCasino, ...data });
    //if field "math" of itemGameCasino is fill, the object is stored on array gamesCasino
    //and itemGameCasino return at original state (empty)
    if (Object.keys(data).filter((key) => key === "math").length > 0) {
      setGamesCasino([...gamesCasino, { ...itemGameCasino, ...data }]);
      setItemGameCasino(initialItemGameCasino);
    }
  };

/*------------FUNCTIONS TO INSTALL GAME ON CASINO ON RESPECTIVE PROVIDER------------------------------*/
  /*
  * FIRST STAGE
  * Function to upload game on provider 
  * body: array with 1 object ( 1 parameter: version_id of game that was selected)
  */
  const uploadGameToProvider = () => {
    //get the version of the game selected in "const - gamesCasino"
    const object = gamesCasino.map((version) => version.version);
    
    /*
    * url whose name of casino and object (version_id of game) are sent as parameters
    * axios.post('/api/providers/' + {nameProvider} + '/games/', object)
    */
    
    /*
    * url whose name of casino and object (version_id of game) are manually put 
    * axios.post('/api/providers/icarus/games/baccarat_E7.2.11-G1.1.1-I3.1.0-FMQ2.1.0_RC')
    */

    /*
    * url whose name of casino are manually put and object (version_id of game) obtained by function above  
    */
    axios.post('api/providers/icarus/games/', object)
      .then((response) => {
        console.log(response.data)
      }).catch((err) => { 
        console.log('Something bad is happened:', err.response) 
    })    
  }

  /*
  * SECOND STAGE
  * Function to get default configs of the game uploaded
  * save object on memory (nameGame = versions.id)
  */
  const getDefaultConfigOfGame = ()=> {
    //get the version of the game selected in "const gamesCasino"
    const object = gamesCasino.map((version) => version.version);

    /*
    * url whose name of casino and object (version_id of game) are sent as parameters
    * axios.get('/api/configs?casino=' + {nameCasino} + '&game=' + object) 
    */

    /*
    * url whose name of casino and object (version_id of game) are manually put 
    * axios.get('/api/configs?casino=icarus&game=baccarat_E7.2.11-G1.1.1-I3.1.0-FMQ2.1.0_RC')
    */

    /*
    * url whose name of casino are manually put and object (version_id of game) obtained by function above  
    */
    axios.get('/api/configs?casino=icarus&game=' + object)
      .then((response) => {
        console.log(response);
        console.log(response.data);
        //save obtained result on const "defaultConfigs"
        setDefaultConfigsGame(response.data);
      }).catch((err) => { 
        console.log('Something bad is happened:', err.response) 
      }) 
  };
    
/*
  * THIRD STAGE
  * Function to upload maths on provider
  * POST [array of strings with maths selected for game - "math_paths"]
  * strings = math's description
  */
  const uploadMathToProvider = () => {
    //array of string thats store maths_path of the game selected
    //obtained by function getConfigsDefaultOfGame (GameSpec -> math_paths [excludes '../../' & includes'...'(example:"MATH-BACCARAT_SV-0.3.38-RELEASE_64.so","...")])
    getDefaultConfigOfGame();
    const mathsPaths = [];
    /* mathsPaths = mathsPaths.push(
      defaultConfigs.map((paths) => paths.GameSpec.map((mathPath) => mathPath.math_paths))
    ) 
    return (console.log(mathsPaths)); */
   
    //const object = gamesCasino.map((math) => math.math);
    //axios.post('/api/providers/' + {nameProvider} + '/maths', object)
    /* axios.post('/api/providers/icarus/maths', mathsPaths)
      .then((response) => {
        console.log(response.data)
      }).catch((err) => { 
        console.log('Something bad is happened:', err.response) 
      })  */
  }

/*
  * FOURTH STAGE
  * Function to upload configs of game to save on provider
  * POST [array of strings with maths selected for game - "math_paths"]
  */
  const uploadConfigToProvider = () => {
    /* const object = {

    }
    //axios.post('/api/providers/' + {nameProvider} + '/configs/', object)
    axios.post('/api/providers/icarus/configs', object)
      .then((response) => {
        console.log(response.data)
      }).catch((err) => { 
        console.log('Something bad is happened:', err.response) 
    }) */
  } 

/*-------------------------------------------------------------------------------------------------- */
  
  /*
   * Function to complete install game on casino (on respective provider)
   * One POST to each game
   * (call 4 stages above)
   */
  
  const addGamesCasinoToApi = (nameProvider) => {
    //uploadGameToProvider(); 
    uploadMathToProvider();
    //uploadConfigToProvider();
  };

  /*
   * Function to clear all data from table of games addicted
   */
  const resetTable = (i) => {
    setGamesCasino(gamesCasino.slice(i,0));
  } 

  /*
   * Function to filter games by search bar
   */
  useEffect(() => {
    if (typeof games !== "undefined" && games !== null) {
      setFilteredGames(
        games.filter((game) => {
          return game.game_name.toLowerCase().includes(searchGames.toLowerCase());
        })
      );
    }
  }, [searchGames, games]);

  return (
    <>
      <Container className="page_container">
        <Row className="page_row">
          <Col className="page_col_top">
            <div className="page_head">
              <h1 className="page_head_title">  
                Games
              </h1>
            </div>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="basic-addon1">
                  <i className="pe-7s-search"></i>
                </InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                aria-label="Username"
                aria-describedby="basic-addon1"
                onChange={(e) => {
                  setSearchGames(e.target.value);
                  setCurrentPage("1");
                }}
              />
            </InputGroup>
            <OverlayTrigger
              trigger={['hover', 'focus']}
              key="left"
              placement="left"
              overlay={
                <Tooltip id={`tooltip-left`}>
                  <strong>Add Game(s) To Casino</strong>.
                </Tooltip>
              }
            >
            <div className="page_popup">
              <div className="page_show_popup">
                <div className="page_show_popup_div">
                  <i  
                    className="pe-7s-look"
                    onClick={() => setShowModalTable(true)}
                  >
                  </i>
                </div>
              </div>
            </div>
            </OverlayTrigger>
            <Modal 
              className="app_modal"
              show={showModalTable} 
              onHide={() => {
                setShowModalTable(false);
              }}
            >
              <Modal.Header className="app_modal_header" closeButton>
                <Modal.Title className="app_modal_header_title">
                  ADD GAME(S) TO CASINO
                </Modal.Title>
              </Modal.Header>
              <Modal.Body className="app_modal_body">
                <Table striped bordered hover responsive="sm">
                  <thead>
                    <tr>
                      <th>Game</th>
                      <th>Version</th>
                      <th>Math</th>
                    </tr>
                  </thead>
                  <tbody>
                  {gamesCasino.map((game,i) => (
                    <tr key={i}>
                      <td>{game.game}</td>
                      <td>{game.version}</td>
                      <td>{game.math}</td>
                    </tr>
                  ))}
                  </tbody>
                </Table>
                <Button
                  className="button_table_gamesToCasino"
                  variant="success"
                  type="submit"
                  onClick={(i) => {
                    addGamesCasinoToApi();
                    resetTable(i);
                    setShowModalTable(false);
                  }}
                >
                Submit
                </Button>
              </Modal.Body>
              {/* <Modal.Footer className="app_modal_footer">
                <Button
                  className="app_modal_footer_button"
                  onClick={() => {
                    setShowModalTable(false);
                  }}
                >Close
                </Button>
              </Modal.Footer> */}
            </Modal>
            {loading ? (
              <Spinner animation="border" variant="primary">
                <span className="sr-only">Loading...</span>
              </Spinner>
            ) : (
              <Row className="page_row">
                {filteredGames
                    .sort((a, b) => (a.game_name > b.game_name ? 1 : -1))
                    .slice(
                      (currentPage - 1) * gamesPerPage,
                      currentPage * gamesPerPage
                    )
                    .map((game) => {
                      return (
                        <Col lg={2} md={3} xs={12} className="page_col_list" key={game.game_name}>
                          <div
                            className="page_list_div"
                            onClick={() => {
                              updateItemGameCasino({ game: game.game_name });
                              handleClickInfo(game.game_name);
                              handleShowModal();
                            }}
                          >
                            {game.game_name}
                          </div>
                          {
                            expandedGames.includes(game.game_name) && (
                              <Modal
                                className="app_modal"
                                show={showModal}
                                onHide={(e) => {
                                  handleCloseModal(game.game_name);
                                }}
                              >
                                <Modal.Header className="app_modal_header" closeButton>
                                  <Modal.Title className="app_modal_header_title">
                                    {game.game_name.toUpperCase()}
                                  </Modal.Title>
                                </Modal.Header>
                                <Modal.Body className="app_modal_body">
                                  {gameModalShow ? (
                                    <GamesInfo
                                      value={game}
                                      updateItemGameCasino={
                                        updateItemGameCasino
                                      }
                                      onClose={(e) => {
                                        setGameModalShow(false);
                                        setMathModalShow(true);
                                      }}
                                    />
                                  ) : null}
                                  {mathModalShow ? (
                                    <GameMaths
                                      gameType={game.game_type}
                                      updateItemGameCasino={
                                        updateItemGameCasino
                                      }
                                      onClose={() => {
                                        handleCloseModal(game.game_name);
                                      }}
                                    />
                                  ) : null}
                                </Modal.Body>
                                {/* <Modal.Footer className="app_modal_footer">
                                  <Button
                                    className="app_modal_footer_button"
                                    onClick={(e) => {
                                      handleCloseModal(game.game_name);
                                    }}
                                  >
                                    Close
                                  </Button>
                                </Modal.Footer> */}
                              </Modal>
                            )
                          }
                        </Col>
                      );
                    })}
              </Row>
            )}
            <PaginationList
              itemsPerPage={gamesPerPage}
              totalItems={filteredGames.length}
              currentPage={currentPage}
              setCurrentPage={setCurrentPage}
            />
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default GamesList;
