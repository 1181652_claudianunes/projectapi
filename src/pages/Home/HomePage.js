import React, { useContext } from "react";
import { Container, Row, Col, Card, Spinner } from "react-bootstrap";

import '../../styles/Dashboard/dashboard.css';

import { GamesContext } from "../../contexts/GamesContext";
import { MathsContext } from "../../contexts/MathsContext";
import { ProvidersContext } from "../../contexts/ProvidersContext";
import { CasinosContext } from "../../contexts/CasinosContext";

//-----------------------------------------------------------------------------------------------------
//                             FUNCTIONS TO GET STATISTICS BY PAGE
//-----------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------
// GAMES
//-----------------------------------------------------------------------------------------------------

/*
* Function to return count of all games
*/
const getTotalGames = (games) => {
  if (typeof games !== "undefined" && games !== null){
    return (games.length);
  }else{
    return 0;
  }
};

/*
* Function to return count of games with version whose status equals to "CLOSED", "GOLD" or"RC" 
*/
const arrStatus = ["CLOSED","GOLD","RC"];
const getGamesVersion = (games, status) => {
  let versions = [];
  games.forEach((game) => {
    arrStatus.forEach((status) => {
      versions = versions.concat(game.versions.filter((item) => {
        //just search for versions that haven't yet been filtered by status
        return (item.status === status && (versions.filter(item2 => {
          return (item2.version === item.version)
        })))
      }));
    });
  })
  return (versions.filter((x) => x.status === status).length)
}

//-----------------------------------------------------------------------------------------------------
// MATHS
//-----------------------------------------------------------------------------------------------------

/*
* Function to return count of all maths
*/
const getTotalMaths = (maths) => {
  if (typeof maths !== "undefined" && maths !== null){
    return (maths.length);
  }else{
    return 0;
  }
};

//-----------------------------------------------------------------------------------------------------
// PROVIDERS
//-----------------------------------------------------------------------------------------------------

/*
* Function to return count of all providers
*/
const getTotalProviders = (providers) => {
  if (typeof providers !== "undefined" && providers !== null){
    return (providers.length);
  }else{
    return 0;
  }
};

//-----------------------------------------------------------------------------------------------------
// CASINOS
//-----------------------------------------------------------------------------------------------------

/*
* Function to return count of all casinos of provider
*/
const getTotalCasinos = (casinos) => {
  if (typeof casinos !== "undefined" && casinos !== null){
    return (casinos.length);
  }else{
    return 0;
  }
};

//-----------------------------------------------------------------------------------------------------
//                                          STATISTICS DASHBOARD
//-----------------------------------------------------------------------------------------------------

const Dashboard = () => {
  //games by method GET from API (Context)
  const gamesContext = useContext(GamesContext);
  const { games, loading } = gamesContext;
  //maths by method GET from API (Context)
  const mathsContext = useContext(MathsContext);
  const { maths } = mathsContext;
  //providers by method GET from API (Context)
  const providersContext = useContext(ProvidersContext);
  const { providers } = providersContext;
  //casinos by method GET from API (Context)
  const casinosContext = useContext(CasinosContext);
  const { casinos } = casinosContext;

  return (
    <>
      <Container className="page_container">
        <Row className="page_row">
          <Col className="page_col_top">
          <div className="page_head">
              <h1 className="page_head_title">  
                Dashboard
              </h1>
            </div>
          </Col>
        </Row>
        <Row className="page_row">
          <Col className="dashboard_col_cards">
            <Card className="dashboard_card" >
              <Card.Header className="dashboard_card_header">
                <a href="/games" className="dashboard_card_header_a">
                  <div className="dashboard_card_header_icon">
                    <div className="dashboard_card_header_icon_a">
                      <div className="dashboard_card_header_icon_a_i">
                        <i className="pe-7s-joy"></i>
                      </div>
                    </div>
                  </div>
                  <h3 className="dashboard_card_header_title">Games</h3>
                </a>
              </Card.Header>
                {loading ? (
                  <Spinner animation="border" variant="primary" className="dashboard_card_header_loading">
                    <span className="sr-only">Loading...</span>
                  </Spinner>
                ) : (
                <Card.Body className="dashboard_card_body">
                <ul className="dashboard_card_body_ul">
                  <li className="dashboard_card_body_li">
                    Total Games:
                    <span className="dashboard_card_body_li_count">
                      {getTotalGames(games)}
                    </span>
                  </li>
                  <li className="dashboard_card_body_li">
                    By Game Type:
                      <p className="dashboard_card_body_li_2">
                        Baccarat
                          <span className="dashboard_card_body_li_count_2">
                            {games.filter((x) => {
                              return (
                                x.game_type === "BACCARAT" 
                              )
                            }).length}
                          </span>
                      </p>
                      <p className="dashboard_card_body_li_2">
                        Banca Francesa
                          <span className="dashboard_card_body_li_count_2">
                            {games.filter((x) => {
                              return (
                                x.game_type === "BANCAFRANCESA" 
                              )
                            }).length}
                          </span>
                      </p>
                      <p className="dashboard_card_body_li_2">
                        Bingo
                          <span className="dashboard_card_body_li_count_2">
                            {games.filter((x) => {
                              return (
                                x.game_type === "BINGO" || x.game_type === "LOTTO"
                              )
                            }).length}
                          </span>
                      </p>
                      <p className="dashboard_card_body_li_2">
                        BlackJack
                          <span className="dashboard_card_body_li_count_2">
                            {games.filter((x) => {
                              return (
                                x.game_type === "BLACKJACK"
                              )
                            }).length}
                          </span>
                      </p>
                      <p className="dashboard_card_body_li_2">
                        Poker
                          <span className="dashboard_card_body_li_count_2">
                            {games.filter((x) => {
                              return (
                                x.game_type === "POKER"
                              )
                            }).length}
                          </span>
                      </p>
                      <p className="dashboard_card_body_li_2">
                        Slots
                          <span className="dashboard_card_body_li_count_2">
                            {games.filter((x) => {
                              return (
                                x.game_type === "SLOTS"
                              )
                            }).length}
                          </span>
                      </p>
                    </li>
                    <li className="dashboard_card_body_li">
                      By Version Status:
                        <p className="dashboard_card_body_li_2">
                          CLOSED
                            <span className="dashboard_card_body_li_count_2">
                              {getGamesVersion(games, 'CLOSED')}
                            </span>
                        </p>
                        <p className="dashboard_card_body_li_2">
                          GOLD
                            <span className="dashboard_card_body_li_count_2">
                              {getGamesVersion(games, 'GOLD')}
                            </span>
                        </p>
                        <p className="dashboard_card_body_li_2">
                          RC
                            <span className="dashboard_card_body_li_count_2">
                              {getGamesVersion(games, 'RC')}
                          </span>
                        </p>
                    </li>
                </ul>
              </Card.Body>
              )}
            </Card>
          </Col>
          <Col lg={3} md={6} xs={12} className="dashboard_col_cards">
            <Card className="dashboard_card">
              <Card.Header className="dashboard_card_header">
                <a href="/maths" className="dashboard_card_header_a">
                  <div className="dashboard_card_header_icon">
                    <div className="dashboard_card_header_icon_a_i">
                        <i className="pe-7s-calculator"></i>
                      </div>
                  </div>
                  <h3 className="dashboard_card_header_title">Maths</h3>
                </a>
              </Card.Header>
              {loading ? (
                  <Spinner animation="border" variant="primary" className="dashboard_card_header_loading">
                    <span className="sr-only">Loading...</span>
                  </Spinner>
              ) : (
              <Card.Body className="dashboard_card_body">
                <ul className="dashboard_card_body_ul">
                  <li className="dashboard_card_body_li">
                    Total Maths:
                      <span className="dashboard_card_body_li_count">
                      {getTotalMaths(maths)}
                    </span>
                  </li>
                  <li className="dashboard_card_body_li">
                    By Game Type:
                      <p className="dashboard_card_body_li_2">
                      Baccarat
                        <span className="dashboard_card_body_li_count_2">
                          {maths.filter((x) => {
                            return (
                              x.game_type === "baccarat"
                            )
                          }).length}
                        </span>
                      </p>
                      <p className="dashboard_card_body_li_2">
                        Banca Francesa
                          <span className="dashboard_card_body_li_count_2">
                            {maths.filter((x) => {
                              return (
                                x.game_type === "banca_francesa"
                              )
                            }).length}
                          </span>
                      </p>
                      <p className="dashboard_card_body_li_2">
                        Bingo
                          <span className="dashboard_card_body_li_count_2">
                            {maths.filter((x) => {
                              return (
                                x.game_type === "bingo"
                              )
                            }).length}
                          </span>
                      </p>
                      <p className="dashboard_card_body_li_2">
                        BlackJack
                          <span className="dashboard_card_body_li_count_2">
                            {maths.filter((x) => {
                              return (
                                x.game_type === "blackjack"
                              )
                            }).length}
                          </span>
                      </p>
                      <p className="dashboard_card_body_li_2">
                        Poker
                          <span className="dashboard_card_body_li_count_2">
                            {maths.filter((x) => {
                              return (
                                x.game_type === "video_poker"
                              )
                            }).length}
                          </span>
                      </p>
                      <p className="dashboard_card_body_li_2">
                        Slots
                          <span className="dashboard_card_body_li_count_2">
                            {maths.filter((x) => {
                              return (
                                x.game_type === "spins"
                              )
                            }).length}
                          </span>
                      </p>
                  </li>
                  <li className="dashboard_card_body_li">
                    By Status:
                      <p className="dashboard_card_body_li_2">
                      Debug
                        <span className="dashboard_card_body_li_count_2">
                          {maths.filter((x) => {
                            return (
                              x.is_debug === true
                            )
                          }).length}
                        </span>
                      </p>
                      <p className="dashboard_card_body_li_2">
                        Release
                          <span className="dashboard_card_body_li_count_2">
                            {maths.filter((x) => {
                              return (
                                x.is_debug === false
                              )
                            }).length}
                          </span>
                      </p>
                  </li>
                </ul>
              </Card.Body>
              )}
            </Card>
          </Col>
          <Col lg={3} md={6} xs={12} className="dashboard_col_cards">
            <Card className="dashboard_card">
              <Card.Header className="dashboard_card_header">
                <a href="/providers" className="dashboard_card_header_a">
                  <div className="dashboard_card_header_icon">
                    <div className="dashboard_card_header_icon_a">
                      <div className="dashboard_card_header_icon_a_i">
                        <i className="pe-7s-server"></i>
                      </div>
                    </div>
                  </div>
                  <h3 className="dashboard_card_header_title">Providers</h3>
                </a>
              </Card.Header>
              {loading ? (
                  <Spinner animation="border" variant="primary" className="dashboard_card_header_loading">
                    <span className="sr-only">Loading...</span>
                  </Spinner>
              ) : (
              <Card.Body className="dashboard_card_body">
                <ul className="dashboard_card_body_ul">
                  <li className="dashboard_card_body_li">
                    Total Providers:
                    <p className="dashboard_card_body_li_count">
                      { getTotalProviders(providers) }
                    </p>
                  </li>
                </ul>
              </Card.Body>
              )}
            </Card>
          </Col>
          <Col lg={3} md={6} xs={12} className="dashboard_col_cards">
            <Card className="dashboard_card">
              <Card.Header className="dashboard_card_header">
                <a href="/casinos" className="dashboard_card_header_a">
                  <div className="dashboard_card_header_icon">
                    <div className="dashboard_card_header_icon_a_i">
                        <i className="pe-7s-user"></i>
                      </div>
                  </div>
                  <h3 className="dashboard_card_header_title">Casinos</h3>
                </a>
              </Card.Header>
              {loading ? (
                  <Spinner animation="border" variant="primary" className="dashboard_card_header_loading">
                    <span className="sr-only">Loading...</span>
                  </Spinner>
                ) : (
              <Card.Body className="dashboard_card_body">
                <ul className="dashboard_card_body_ul">
                  <li className="dashboard_card_body_li">
                    Total Casinos:
                    <p className="dashboard_card_body_li_count">
                      {getTotalCasinos(casinos)}
                    </p>
                  </li>
                </ul>
              </Card.Body>
              )}
            </Card>
          </Col>
        </Row>  
      </Container>
    </>                        
  );
}

export default Dashboard;
