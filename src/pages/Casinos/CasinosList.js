import React, { useContext } from "react";
import { Container, Row, Col, InputGroup, FormControl, Spinner } from "react-bootstrap";

import { CasinosContext } from "../../contexts/CasinosContext";

//import PaginationList from "../components/Pagination";
//import CasinosInfo from "../components/Casinos/CasinosInfo.js";
//import AddCasino from "../components/Modals/AddCasino";

const CasinosList = ( casino ) => {
    //casinos by method GET from API (Context)
    const casinosContext = useContext(CasinosContext);
    const { casinos, loading } = casinosContext;
    
    //casino selected and filtered by search bar 
    /* const [expandedCasinos, setExpandedCasinos] = useState([]);
    const [searchCasinos, setSearchCasinos] = useState("");
    const [filteredCasinos, setFilteredCasinos] = useState([]);  */

    //modal to show info about  selected casino
    /* const [showModal, setShowModal] = useState(false); */
    
    //modal to add new casino
    /* const [addModalShow, setAddModalShow] = useState(false); */
    
    //pagination
    /* const [currentPage, setCurrentPage] = useState(1);
    const [casinosPerPage] = useState(18); */

    
    /*
    * Function to open the modal 
    */
    /* const handleShowModal = () => {
        setShowModal(true);
    } */

    /*
    * Function to close the modal 
    */
    /* const handleCloseModal = (itemCasino) => {
        handleClickInfo(itemCasino);
        setShowModal(false);
    } */

    /** 
     * Function to filter and show provider when it is clicked
     * itemProvider is the DOM element that triggered the OnClick event.
    */
    /* const handleClickInfo = (itemCasino) => {
        //if casino clicked is included on expandedCasinos, filterCasinos delete repeat item
        if (expandedCasinos.includes(itemCasino)){
            const filterCasinos = expandedCasinos.filter((expandedCasino) => expandedCasino !== itemCasino);
            setExpandedCasinos(filterCasinos);
        }else {
            setExpandedCasinos([itemCasino, ...expandedCasinos]);
        }
    }  */
    
    /*
    * Function to filter providers by search bar 
    */
    /* useEffect(() => {
        if (typeof casinos !== "undefined" && casinos !== null)  {
            setFilteredCasinos(
                casinos.filter((casino) => {
                    return casino.name.toLowerCase().includes(searchCasinos.toLowerCase())
                })
            );
        }
    }, [searchCasinos, casinos]); */

    //RETURNS CASINOS LIST OF PROVIDER
    return (
        <>
            <Container className="page_container">
                <Row className="page_row">
                    <Col className="page_col_top">
                        <div className="page_head">
                            <h1 className="page_head_title">  
                                Casinos
                            </h1>
                        </div>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="basic-addon1">
                                    <i className="pe-7s-search"></i>
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                                aria-label="Username"
                                aria-describedby="basic-addon1"
                                /* onChange={ e => {setSearchCasinos(e.target.value); setCurrentPage("1")}} */
                            />
                        </InputGroup>
                        {loading ? (
                            <Spinner animation="border" variant="primary">
                                <span className="sr-only">Loading...</span>
                            </Spinner>
                        ) : (
                            <Row className="page_row">
                                <Col lg={2} md={3} xs={12} className="page_col_list">
                                    <div
                                        className="page_list_div"
                                    >{casinos}
                                    </div>
                                </Col>   
                            </Row>                         
                        )
                        /*<Button
                            style={{display: 'flex', float: 'left'}}
                            className={(handleAddGameCasino() ? "enabled" : "disabled")}
                            variant="success"
                        >Add Casino(s) To Provider
                        </Button>
                        <br />
                        <br /> 
                        /* <Button 
                            className="mt-auto"
                            variant="primary" 
                            onClick={() => setAddModalShow(true)}
                        >Add Casino
                        </Button>
                        <AddCasino
                            show={addModalShow}
                            onHide={() => {
                                if(window.confirm("Are you sure to cancel creation of the casino?")) {
                                    setAddModalShow(false);
                                }
                            }}
                        />
                        <br />
                        <br />
                        <InputGroup className="mb-4">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="basic-addon1">
                                    Search Casino:
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                                aria-label="Username"
                                aria-describedby="basic-addon1"
                                onChange={ e => {setSearchCasinos(e.target.value); setCurrentPage("1")}}
                            />
                        </InputGroup>
                        {loading ? ('Loading...') : (
                            <Container fluid="md">
                                <Row md={3}>
                                    {filteredCasinos.sort((a, b) => a.name > b.name ? 1 : -1).slice(((currentPage-1)*casinosPerPage), ((currentPage*casinosPerPage))).map((casino) => {
                                        return (
                                            <Col l={6} key={casino.name}>
                                                <Button 
                                                    className="xl"
                                                    variant="outline-secondary"
                                                    onClick={() => {
                                                        handleClickInfo(casino.name);
                                                        handleShowModal();
                                                    }}
                                                    block
                                                >{casino.name}
                                                </Button>
                                            </Col>
                                        ); 
                                    })}
                                </Row>
                            </Container>
                        )} 
                        <br /> 
                        <PaginationList 
                            itemsPerPage={casinosPerPage} 
                            totalItems={filteredCasinos.length}  
                            currentPage={currentPage}
                            setCurrentPage={setCurrentPage}
                        />*/}
                    </Col>
                </Row>
            </Container>
        </>
    )
}   

export default CasinosList;
