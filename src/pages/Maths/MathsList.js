import React, { useContext, useState, useEffect } from "react";
import { Container, Row, Col, Button, InputGroup, FormControl, Modal, Spinner } from "react-bootstrap";

import { MathsContext } from "../../contexts/MathsContext";

import PaginationList from "../../components/Pagination";
import MathsByGameType from "../../components/Maths/MathsByGameType";


const MathsList = ( math ) => {
    //maths by method GET from API (Context), for gameslist presentation
    const mathsContext = useContext(MathsContext);
    const { maths, loading } = mathsContext;

    //game types existing 
    const [expandedGameTypes, setExpandedGameTypes] = useState([]);

    //maths selected and filtered by search bar 
    const [searchMaths, setSearchMaths] = useState("");
    const [filteredMaths, setFilteredMaths] = useState([]);
    
    //pagination
    const [currentPage, setCurrentPage] = useState(1);
    const [gameTypesPerPage] = useState(18);

    //modal to show all maths for game type selected
    const [showModal, setShowModal] = useState(false);

    /*
    * Function to close the modal 
    */
    const handleCloseModal = (itemGameType) => {
        handleClickGameType(itemGameType);
        setShowModal(false);
    }
    
    /*
    * Function to open the modal 
    */
    const handleShowModal = () => {
        setShowModal(true);
    }

    /*
    * Function to group games by Type 
    */
    // Accepts the array "maths" and key "game_type"
    const groupByGameType = (maths, game_type) => {
        // Return the end result
        return (
            maths.reduce((types,currentValue) => {
                types[currentValue[game_type]] = [...(types[currentValue[game_type]] || []),currentValue];
                return types;
            }, {})
        );
    }
    
    /*
    * Function to filter maths by search bar 
    */
    useEffect(() => {
        if (typeof maths !== "undefined" && maths !== null)  {
            const gameTypes = groupByGameType(maths, 'game_type');
            const mathsGrouped = Object.entries(gameTypes).filter((math) => {
                    return math[0]
                        .toString()
                        .toLowerCase()
                        .includes(searchMaths.toString().toLowerCase())
            });
            setFilteredMaths(mathsGrouped);
        }
    }, [searchMaths, maths]); 
    
    /*
    * Function to filter and show maths description for each game type 
    */
    const handleClickGameType = (itemGameType) => {
        if (expandedGameTypes.includes(itemGameType)){
            const filterGameTypes = expandedGameTypes.filter(
                (expandedGameType) => expandedGameType !== itemGameType
            );
            setExpandedGameTypes(filterGameTypes);
        }else {
            setExpandedGameTypes([itemGameType, ...expandedGameTypes]);
        }
    }

    return (
        <>
            <Container className="page_container">
                <Row className="page_row">
                    <Col className="page_col_top">
                        <div className="page_head">
                            <h1 className="page_head_title">  
                                Maths
                            </h1>
                        </div>
                        <InputGroup size="l" className="mb-3" >
                            <InputGroup.Prepend>
                                <InputGroup.Text> 
                                    <i className="pe-7s-search"></i>
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                                aria-label="Username"
                                aria-describedby="basic-addon1"
                                onChange={ e => {setSearchMaths(e.target.value); setCurrentPage("1")}}
                            />
                        </InputGroup>
                        {loading ? (
                            <Spinner animation="border" variant="primary">
                                <span className="sr-only">Loading...</span>
                            </Spinner>
                        ) : (
                            <Row className="page_row">
                                {filteredMaths.slice(((currentPage-1)*gameTypesPerPage), ((currentPage*gameTypesPerPage))).map(([type,math]) => {
                                        return (
                                            <Col lg={2} md={3} xs={12} className="page_col_list" key={type}>
                                                <div
                                                    className="page_list_div"
                                                    onClick={() => {
                                                        handleClickGameType(type);
                                                        handleShowModal();
                                                    }}
                                                >{type}
                                                </div>
                                            {expandedGameTypes.includes(type) && 
                                                <Modal show={showModal} onHide={(e) => {handleCloseModal(type)}}>
                                                    <Modal.Header className="app_modal_header" closeButton>
                                                        <Modal.Title className="app_modal_header_title">
                                                            {type.toUpperCase()}
                                                        </Modal.Title>
                                                    </Modal.Header>
                                                    <Modal.Body className="app_modal_body">
                                                        <MathsByGameType maths={maths} />
                                                    </Modal.Body>
                                                    {/* <Modal.Footer className="app_modal_footer">
                                                        <Button 
                                                            className="app_modal_footer_button"
                                                            onClick={(e) => {handleCloseModal(type)}}
                                                        >Close
                                                        </Button>
                                                    </Modal.Footer> */}
                                                </Modal>
                                            }
                                            </Col>
                                        );
                                    })}
                            </Row>
                        )}
                        <br />
                        <PaginationList 
                            itemsPerPage={gameTypesPerPage} 
                            totalItems={filteredMaths.length} 
                            currentPage={currentPage}
                            setCurrentPage={setCurrentPage}
                        />
                    </Col>
                </Row>
            </Container>
        </>
    );   
};   

export default MathsList;
